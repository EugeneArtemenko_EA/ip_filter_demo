package com.example.demo.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController {

    @GetMapping("/app")
    public ResponseEntity<?> getApp() {

        return new ResponseEntity<>("Allowed IP Address ", HttpStatus.OK);
    }
}
